// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"


void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick( DeltaTime );
	//TODO create AimTowardPlayer Function

}

ATank* ATankAIController::GetControlledTank() const
{
	return Cast<ATank>( GetPawn() );
}

void ATankAIController::BeginPlay()
{
	auto ControlledTank = GetControlledTank();
	if (!ControlledTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("Controller IS NOT posessing anything"));
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("AI posessing %s"), *(ControlledTank->GetName()));

	}


	Super::BeginPlay();
}

ATank * ATankAIController::GetPlayerTank() const
{
	auto  PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	if (!PlayerPawn) { return nullptr; }
	return Cast<ATank>(PlayerPawn);
}