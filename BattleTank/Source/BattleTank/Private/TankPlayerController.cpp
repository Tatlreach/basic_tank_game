#include "TankPlayerController.h"

ATank* ATankPlayerController::GetControlledTank() const
{
	return Cast<ATank>(GetPawn());
}

void ATankPlayerController::BeginPlay()
{
	auto ControlledTank = GetControlledTank();
	if (!ControlledTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("PlayerController IS NOT posessing anything"));
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("PlayerController posessing %s"), *(ControlledTank->GetName()));

	}
	Super::BeginPlay();
}

void ATankPlayerController::AimTowardCrosshair()
{
	if (!GetControlledTank()) {
		UE_LOG(LogTemp, Warning, TEXT("Aim Function Could not find anything"));
		return;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Aim Function found something"));

	}



	FVector HitLocation;
	if (GetSightRayHitLocation(HitLocation))		// is goiing to line trace
	{
		UE_LOG(LogTemp, Warning, TEXT("PlayerController aiming at %s"), *(HitLocation.ToString()));
		//TODO Tell controlled tank to aim at this point
	}
	//get world location if 

}

void ATankPlayerController::Tick(float DeltaTime)
{
	AimTowardCrosshair();	//TODO create AimTowardCrosshair Function
	Super::Tick(DeltaTime);

}

//returns true if 
bool ATankPlayerController::GetSightRayHitLocation(FVector &HitLocation) const
{
	bool bHasHitSomething = true;
	HitLocation = FVector(1.0);

	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize( ViewportSizeX, ViewportSizeY );

	UPROPERTY(EditAnywhere)
	float CrosshairXLocation = 0.5f;

	UPROPERTY(EditAnywhere)
	float CrosshairYLocation = 0.33333f;

	FVector2D ScreenLocation = FVector2D( CrosshairXLocation*ViewportSizeX, CrosshairYLocation*ViewportSizeY );


	UE_LOG(LogTemp, Warning, TEXT("ScreenLocation: %s"), *(ScreenLocation.ToString()));
	
	//get camera
	//find crosshair position
	//get line trace outward from the camera
	//De-project the screen position of the crosshair to the world direction
	//check against the landscape with the raycast

	return bHasHitSomething;
}
