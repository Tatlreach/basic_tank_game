#pragma once

#include "Tank.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ATankPlayerController : public APlayerController
{
	GENERATED_BODY()

private:
	ATank* GetControlledTank() const;

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	//more the tanks barrel so that the shot would hit where the player intersects with aim
	void AimTowardCrosshair();

	bool GetSightRayHitLocation(FVector &HitLocation) const;
	
};
